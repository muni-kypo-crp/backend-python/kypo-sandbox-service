.. KYPO Sandbox Service documentation master file, created by
   sphinx-quickstart on Wed Apr 29 20:12:06 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KYPO Sandbox Service's documentation!
================================================

This project simplifies manipulation of OpenStack cloud platform for KYPO purposes.
It provides REST calls for manipulation with:

- Sandbox definitions
- Pools of sandboxes
- Sandboxes themselves
- Applying Ansible playbooks on sandbox machines

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   
   kypo.sandbox_common_lib
   kypo.sandbox_definition_app
   kypo.sandbox_ansible_app
   kypo.sandbox_instance_app
   kypo.sandbox_service_project

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
